import '../../libs/pagescroll2id/jquery.malihu.PageScroll2id.min.js';

$('.header__menu-link, .to-top-link').mPageScroll2id({
  offset: 50,
  scrollSpeed: 200,
});
