// Variables
const modalOverlay = document.querySelector('.modal__overlay');
const modalWindows = document.querySelectorAll('.modal__window');
const modalErrorContainer = document.querySelector('.modal__error-container');
const modalSuccessContainer = document.querySelector(
  '.modal__success-container'
);
const subscribeEmailInput = document.querySelector('.subscribe__email-input');
const subscribeButton = document.querySelector('.subscribe__button');

// Event Listeners
subscribeButton.addEventListener('click', validationCheck);

modalOverlay.addEventListener('click', modalsHide);

modalWindows.forEach((el) => {
  el.addEventListener('click', modalsHide);
});

// Functions
function validationCheck() {
  if (!subscribeEmailInput.value) {
    modalErrorShow();
  } else {
    modalSuccessShow();
  }
}

function modalsHide() {
  modalOverlay.classList.remove('active');
  modalErrorContainer.classList.remove('active');
  modalSuccessContainer.classList.remove('active');
}

function modalErrorShow() {
  modalOverlay.classList.add('active');
  modalErrorContainer.classList.add('active');
  subscribeEmailInput.classList.add('active');
}

function modalSuccessShow() {
  modalOverlay.classList.add('active');
  modalSuccessContainer.classList.add('active');
  subscribeEmailInput.classList.remove('active');
}
