const headerHamburger = document.querySelector('.header__hamburger');
const headerMenu = document.querySelector('.header__menu');
const header = document.querySelector('.header');

headerHamburger.addEventListener('click', function () {
  for (let i = 0; i < this.children.length; i++) {
    this.children[i].classList.toggle('active');
  }
  headerMenu.classList.toggle('active');
  header.classList.toggle('active');
});
